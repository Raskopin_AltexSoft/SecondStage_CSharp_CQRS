## C# with CQRS pattern
This project is a simple student/teacher catalog wich implemented CQRS pattern. 
CQRS realized in project (https://gitlab.com/SECONDSTAGE_Csharp/School-CQRS/tree/master/School.CQRS) which include commands, commands handlers, query factory and data bus.