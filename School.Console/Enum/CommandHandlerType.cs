﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Console.Enum
{
    internal enum CommandHandlerType
    {
        Regular = 1,
        Disposable = 2
    }
}
