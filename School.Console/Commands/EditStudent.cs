﻿using School.Common.Core;
using School.Console.Attributes;
using School.Console.Interfaces;
using School.CQRS;
using School.CQRS.Commands;

namespace School.Console.Commands
{
    [CommandName("edit-student")]
    [CommandDescription("edit-student", typeof(Strings))]

    class EditStudent : ICommand
    {
        private DataBus _dataBus;

        public EditStudent(DataBus dataBus)
        {
            _dataBus = dataBus;
        }
        public void Execute()
        {
            System.Console.WriteLine(Strings.EnterStudentsFirtsName);
            var firstName = System.Console.ReadLine();
            System.Console.WriteLine(Strings.EnterStudentsLastName);
            var lastName = System.Console.ReadLine();
            System.Console.WriteLine(Strings.EnterStudentsAge);
            int age;
            while (!int.TryParse(System.Console.ReadLine(), out age))
            {
                System.Console.WriteLine(Strings.InvalidAgeValue);
            }
            _dataBus.Send(new CreateStudent(firstName, lastName, age));
        }
    }
}
