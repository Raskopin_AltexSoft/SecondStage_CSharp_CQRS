﻿

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using School.Common.Core;
using School.Common.Utils.Extensions;
using School.Console.Attributes;
using School.Console.Interfaces;

namespace School.Console.Commands
{
    [CommandName("help")]
    [CommandDescription("help", typeof(Strings))]
    class Help : ICommand
    {
        private readonly IEnumerable<ICommandInfo> _commands;

        public Help(IComponentContext context)
        {
            _commands = context.GetCommands();
        }

        public void Execute()
        {
            _commands.ForEach(ci => System.Console.WriteLine(Strings.CommandFormat, ci.Name, ci.Description.ToUpper(CultureInfo.CurrentCulture)));
        }
    }
}
