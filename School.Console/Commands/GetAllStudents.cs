﻿using School.Console.Interfaces;
using School.CQRS.Interfaces;

namespace School.Console.Commands
{
    class GetAllStudents : ICommand
    {
        private readonly IQueryFactory _queryFactory;

        public GetAllStudents(IQueryFactory queryFactory)
        {
            _queryFactory = queryFactory;
        }
        public void Execute()
        {
            var students = _queryFactory.GetAllStudents();
            foreach (var student in students)
            {
                System.Console.WriteLine("student: {0} {1}", student.LastName, student.FirstName);
            }
        }
    }
}
