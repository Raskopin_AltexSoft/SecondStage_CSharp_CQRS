﻿using School.Common.Core;
using School.Console.Attributes;
using School.Console.Interfaces;
using School.CQRS;
using School.CQRS.Commands;

namespace School.Console.Commands
{
    [CommandName("add-teacher")]
    [CommandDescription("add-teacher", typeof(Strings))]
    class AddNewTeacher : ICommand
    {
        private DataBus _dataBus;

        public AddNewTeacher(DataBus dataBus)
        {
            _dataBus = dataBus;
        }
        public void Execute()
        {
            System.Console.WriteLine(Strings.EnterTeachersFirtsName);
            var firstName = System.Console.ReadLine();
            System.Console.WriteLine(Strings.EnterTeachersLastName);
            var lastName = System.Console.ReadLine();
            System.Console.WriteLine(Strings.EnterTeachersAge);
            int age;
            while (!int.TryParse(System.Console.ReadLine(), out age))
            {
                System.Console.WriteLine(Strings.InvalidAgeValue);
            }
            _dataBus.Send(new CreateTeacher(firstName, lastName, age));
        }
    }
}
