﻿using System;
using School.Common.Core;
using School.Console.Attributes;
using School.Console.Interfaces;

namespace School.Console.Commands
{

    [CommandName("exit")]
    [CommandDescription("exit", typeof(Strings))]

    class Exit : ICommand
    {
        public void Execute()
        {
            Environment.Exit(0);
        }
    }
}
