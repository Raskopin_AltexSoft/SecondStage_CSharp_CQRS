﻿using School.Common.Core;
using School.Console.Attributes;
using School.Console.Interfaces;

namespace School.Console.Commands
{
    [CommandName("clear")]
    [CommandDescription("clear", typeof(Strings))]
    class ClearConsole : ICommand
    {
        public void Execute()
        {
            System.Console.Clear();
        }
    }
}
