﻿using System.Linq;
using School.Common.Core;
using School.Console.Attributes;
using School.Console.Interfaces;
using School.CQRS;
using School.CQRS.Interfaces;
using School.Console.Helpers;


namespace School.Console.Commands
{
    [CommandName("assign-teacher")]
    [CommandDescription("assign-teacher", typeof(Strings))]

    class AssignTeacherToStudent : ICommand
    {
        private readonly IQueryFactory _queryFactory;
        private readonly DataBus _dataBus;
        public AssignTeacherToStudent(DataBus dataBus, IQueryFactory queryFactory)
        {
            _queryFactory = queryFactory;
            _dataBus = dataBus;
        }

        public void Execute()
        {
            var teachers = _queryFactory.GetAllTeachers();
            teachers.PrintPersonList(Strings.AllTeachers);
            var teacher = teachers.PickPerson(Strings.PickTeacherToAssign);
            var students = _queryFactory.GetStudentsWithoutTeacher(teacher);
            if (!students.Any())
            {
                System.Console.WriteLine(Strings.NoStudentsToAssign);
                return;
            }

            students.PrintPersonList(Strings.AllStudents);
            var student = students.PickPerson(Strings.PickStudentToAssign);
            _dataBus.Send(new CQRS.Commands.AssignTeacherToStudent(student, teacher));
        }
    }
}
