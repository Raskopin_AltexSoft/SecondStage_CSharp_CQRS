﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using School.Common.Core;
using School.Console.Attributes;
using School.Console.Interfaces;
using School.CQRS.Interfaces;
using School.Data.Interfaces;

namespace School.Console.Commands
{
    [CommandName("print-teachers")]
    [CommandDescription("print-teachers", typeof(Strings))]

    class PrintTeachersDetailedList : ICommand
    {
        private readonly IQueryFactory _queryFactory;

        public PrintTeachersDetailedList(IQueryFactory queryFactory)
        {
            _queryFactory = queryFactory;
        }

        public void Execute()
        {
            var teachers = _queryFactory.GetTeachersDetailed();
            foreach (var teacher in teachers)
            {
                System.Console.WriteLine(teacher);

                if (!teacher.Students.Any())
                {
                    System.Console.WriteLine(Strings.NestedElementFormat, Strings.NoStudents);
                    continue;
                }

                foreach (var student in teacher.Students.OrderBy(s => s.FirstName))
                {
                    System.Console.WriteLine(Strings.NestedElementFormat, student);
                }
            }
        }
    }
}
