﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using School.Common.Core;
using School.Console.Attributes;
using School.Console.Interfaces;
using School.CQRS.Interfaces;
using School.Data.Interfaces;

namespace School.Console.Commands
{
    [CommandName("print-students")]
    [CommandDescription("print-students", typeof(Strings))]

    class PrintStudentsDetaildeList : ICommand
    {
        private readonly IQueryFactory _queryFactory;

        public PrintStudentsDetaildeList(IQueryFactory queryFactory)
        {
            _queryFactory = queryFactory;
        }

        public void Execute()
        {
            var students = _queryFactory.GetStudentsDetailed();
            foreach (var student in students)
            {
                System.Console.WriteLine(student);

                if (student.Teacher == null)
                {
                    System.Console.WriteLine(Strings.NestedElementFormat, Strings.NoTeacherAssigned);
                    continue;
                }

                System.Console.WriteLine(Strings.NestedElementFormat, student.Teacher);
            }
        }
    }
}
