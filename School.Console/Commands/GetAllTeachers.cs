﻿using School.Console.Interfaces;
using School.CQRS.Interfaces;


namespace School.Console.Commands
{
    class GetAllTeachers : ICommand
    {
        private readonly IQueryFactory _queryFactory;

        public GetAllTeachers(IQueryFactory queryFactory)
        {
            _queryFactory = queryFactory;
        }
        public void Execute()
        {
            var teachers = _queryFactory.GetAllTeachers();
            foreach (var teacher in teachers)
            {
                System.Console.WriteLine("teacher: {0} {1}", teacher.LastName, teacher.FirstName);
            }
        }
    }
}
