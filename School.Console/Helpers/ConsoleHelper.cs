﻿using System.Collections.Generic;
using System.Linq;
using School.Common.Core;
using School.Common.Utils.Extensions;
using School.Data.Entity.Models.BaseEntities;

namespace School.Console.Helpers
{
    internal static class ConsoleHelper
    {
        internal static T PickPerson<T>(this IEnumerable<T> persons, string message) where T : Person
        {
            var personsArray = persons as T[] ?? persons.ToArray();

            System.Console.WriteLine(message);

            while (true)
            {
                int id;
                if (!int.TryParse(System.Console.ReadLine(), out id))
                {
                    System.Console.WriteLine(Strings.InvalidId);
                    continue;
                }

                var selectedPerson = personsArray.FirstOrDefault(p => p.Id == id);
                if (selectedPerson == null)
                {
                    System.Console.WriteLine(Strings.WrongId);
                    continue;
                }

                return selectedPerson;
            }
        }

        internal static string PickEditableField(this Person person, string message)
        {
            var properties = person.GetType().GetProperties();

            System.Console.WriteLine(message);
            var _i = 0;
            foreach (var property in properties)
            {
                _i++;
                System.Console.WriteLine("{0}: {1}", _i, property.Name);
            }
            while (true)
            {
                int number;
                if (!int.TryParse(System.Console.ReadLine(), out number) || number < 0 || number > properties.Count() + 1)
                {
                    System.Console.WriteLine(Strings.WrongProperty);
                    continue;
                }
                return properties[number - 1].Name;
            }
        }

        internal static void PrintPersonList(this IEnumerable<Person> persons, string message)
        {
            System.Console.WriteLine(message);

            persons.OrderBy(p => p.FirstName).ForEach(System.Console.WriteLine);
        }
    }
}
