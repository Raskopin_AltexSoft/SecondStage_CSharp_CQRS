﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using School.Console.Attributes;
using School.Console.Interfaces;

namespace School.Console
{
    internal sealed class CommandInfo : ICommandInfo
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Type CommandType { get; set; }

        internal CommandInfo(Type commandType, CommandNameAttribute nameAttribute,
            CommandDescriptionAttribute descriptionAttribute = null)
        {
            if (commandType == null)
                throw new ArgumentNullException("commandType");

            CommandType = commandType;

            Name = nameAttribute.Name;

            Description = descriptionAttribute != null
                ? descriptionAttribute.Description
                : String.Empty;
        }
    }
}
