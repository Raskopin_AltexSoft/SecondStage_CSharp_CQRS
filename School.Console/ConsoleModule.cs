﻿using Autofac;
using School.Console.Enum;
using School.Console.Handlers;
using School.Console.Interfaces;

namespace School.Console
{
    public sealed class ConsoleModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(DisposableCommandHandler<>))
                .Keyed(CommandHandlerType.Disposable, typeof(IHandler<>));
           builder.RegisterGeneric(typeof(CommandHandler<>))
                .Keyed(CommandHandlerType.Regular, typeof(IHandler<>));
            builder.RegisterAssemblyTypes(ThisAssembly)
                .Where(t => typeof(ICommand).IsAssignableFrom(t));
            builder.RegisterType<AutofacHandlerResolver>()
                .As<IHandlerResolver>();
            builder.RegisterType<CommandDispatcher>()
                .As<ICommandDispatcher>();
        }
    }
}
