﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using School.Console.Interfaces;
using School.CQRS;

namespace School.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            Bootstrapper.Initialize();

            using (var scope = Bootstrapper.Container.BeginLifetimeScope())
            {
                var terminal = scope.Resolve<ITerminal>();


                terminal.Run();
            }
        }
    }
}
