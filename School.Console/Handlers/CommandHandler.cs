﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using School.Console.Interfaces;

namespace School.Console.Handlers
{
    internal sealed class CommandHandler<T> : IHandler<T> where T : ICommand
    {
        private readonly T _command;

        public CommandHandler(T command)
        {
            _command = command;
        }

        public void Handle()
        {
            _command.Execute();
        }
    }
}
