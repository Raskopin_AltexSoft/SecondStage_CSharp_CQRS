﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac.Features.OwnedInstances;
using School.Console.Interfaces;

namespace School.Console.Handlers
{
    internal sealed class DisposableCommandHandler<T> : IHandler<T> where T : ICommand, IDisposable
    {
        private readonly Owned<T> _command;

        public DisposableCommandHandler(Owned<T> command)
        {
            _command = command;
        }

        public void Handle()
        {
            try
            {
                _command.Value.Execute();
            }
            finally
            {
                _command.Dispose();
            }
        }
    }
}
