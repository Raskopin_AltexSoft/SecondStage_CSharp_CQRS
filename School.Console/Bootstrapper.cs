﻿using System.Configuration;
using Autofac;
using School.Common.Core;
using School.Common.Core.ConfigFile;
using School.Data;
using School.Console.Interfaces;
using School.CQRS;
using School.CQRS.Commands;
using School.CQRS.Interfaces;


namespace School.Console
{
    internal static class Bootstrapper
    {
        public static Autofac.IContainer Container { get; private set; }

        public static void Initialize()
        {
            InitializeAppSettings();

            InitializeIocContainer();

            RegisterCommands();
        }

        private static void InitializeIocContainer()
        {
            var builder = new ContainerBuilder();

            RegisterTypes(builder);

            RegisterModules(builder);

            Container = builder.Build();
        }

        private static void RegisterTypes(ContainerBuilder builder)
        {
            builder.RegisterType<Terminal>().As<ITerminal>();
            builder.RegisterType<DataBus>().SingleInstance();
            builder.RegisterType<StudentCommandHandler>();
            builder.RegisterType<TeacherCommandHandler>();
            builder.RegisterType<QueryFactory>().As<IQueryFactory>();
        }

        private static void RegisterModules(ContainerBuilder builder)
        {
            builder.RegisterModule<DataModule>();
            builder.RegisterModule<ConsoleModule>();
        }

        private static void InitializeAppSettings()
        {
            var dataAccessConfig = (DataAccessSection)ConfigurationManager.GetSection("dataAccess");

            AppSettings.SerializationType = dataAccessConfig.Serialization.Type;
            AppSettings.FilePath = dataAccessConfig.Serialization.FilePath;
            AppSettings.StorageType = dataAccessConfig.StorageType;
            AppSettings.ConnectionString = dataAccessConfig.Database.ConnectionString;
        }

        private static void RegisterCommands()
        {
            var bus = Container.Resolve<DataBus>();
            var studentCommands = Container.Resolve<StudentCommandHandler>();
            var teacherCommands = Container.Resolve<TeacherCommandHandler>();
            bus.RegisterHandler<CreateStudent>(studentCommands.Handle);
            bus.RegisterHandler<CreateTeacher>(teacherCommands.Handle);
            bus.RegisterHandler<EditStudent>(studentCommands.Handle);
            bus.RegisterHandler<EditTeacher>(teacherCommands.Handle);
            bus.RegisterHandler<AssignTeacherToStudent>(studentCommands.Handle);
        }
    }
}
