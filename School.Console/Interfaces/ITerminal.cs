﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Console.Interfaces
{
    interface ITerminal
    {
        void Run();
        void RunCommand(string command);
    }
}
