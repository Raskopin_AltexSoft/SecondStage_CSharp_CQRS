﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Console.Interfaces
{
    public interface ICommandDispatcher
    {
        void Dispatch<T>() where T : ICommand;

        void Dispatch(Type commandType);
    }
}
