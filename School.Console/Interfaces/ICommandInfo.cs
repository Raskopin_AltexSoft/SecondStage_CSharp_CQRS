﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Console.Interfaces
{
    public interface ICommandInfo
    {
        string Name { get; set; }
        string Description { get; set; }
        Type CommandType { get; set; }
    }
}
