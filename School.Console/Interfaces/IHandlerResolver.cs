﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Console.Interfaces
{
    public interface IHandlerResolver
    {
        IHandler<T> Resolve<T>() where T : ICommand;
    }
}
