﻿namespace School.Console.Interfaces
{
    public interface ICommand
    {
            void Execute();
    }
}