﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Autofac.Core;
using School.Common.Core;
using School.Console.Commands;
using School.Console.Interfaces;
using School.CQRS;
using School.Data.Repository;
using School.Data.Interfaces;


namespace School.Console
{
    class Terminal : ITerminal
    {
        private readonly ICommandDispatcher _commandDispatcher;
        private readonly IEnumerable<ICommandInfo> _commands;

        public Terminal(ICommandDispatcher commandDispatcher, IComponentContext context)
        {
            _commands = context.GetCommands();
            _commandDispatcher = commandDispatcher;
        }

        public void Run()
        {
             var suffix = Assembly.GetExecutingAssembly().GetName().Name + '>';
             while (true)
             {
                 System.Console.Write(suffix);
                 var command = System.Console.ReadLine();
                 RunCommand(command);
             }
        }

        public void RunCommand(string command)
        {
            if (string.IsNullOrWhiteSpace(command))
                return;


            var commandInfo = _commands
                .FirstOrDefault(c => c.Name.Equals(command.Trim(), StringComparison.CurrentCultureIgnoreCase));

            if (commandInfo == null)
            {
                System.Console.WriteLine(Strings.CommandDoesNotExist);
                return;
            }
            var container = Bootstrapper.Container;
            var queryFactory = new QueryFactory(container.Resolve<ITeacherRepository>(), container.Resolve<IStudentRepository>());

            _commandDispatcher.Dispatch(commandInfo.CommandType);
       
        }

    }
}
