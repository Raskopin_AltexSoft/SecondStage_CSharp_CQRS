﻿using System.Linq;
using School.CQRS.Interfaces;
using School.Data.Entity.Models;
using School.Data.Interfaces;

namespace School.CQRS
{
    public class QueryFactory : IQueryFactory
    {
        private readonly IStudentRepository _studentRepository;
        private readonly ITeacherRepository _teacherRepository;

        public QueryFactory(ITeacherRepository teacherRepository, IStudentRepository studentRepository)
        {
            if (teacherRepository != null)
            _teacherRepository = teacherRepository;
            if (studentRepository != null)
                _studentRepository = studentRepository;
        }
        public Teacher[] GetAllTeachers()
        {

            return _teacherRepository.GetAll().ToArray();
        }

        public Student[] GetAllStudents()
        {
            return _studentRepository.GetAll().ToArray();
        }

        public Student[] GetStudentsWithoutTeacher(Teacher teacher)
        {
            return _studentRepository.GetList(s => teacher.Id != s.TeacherId).ToArray();
        }
    
        public Student[] GetStudentsDetailed()
        {
            return _studentRepository.GetAllIncludeTeacher().OrderBy(t => t.FirstName).ToArray();
        }

        public Teacher[] GetTeachersDetailed()
        {
            return _teacherRepository.GetAllIncludeStudents().OrderBy(t => t.FirstName).ToArray();
        }
    }
}
