﻿using School.Data.Entity.Models;
using School.Data.Interfaces;
using School.CQRS.Commands;

namespace School.CQRS
{
    public class StudentCommandHandler
    {
        private readonly IStudentRepository _repository;

        public StudentCommandHandler(IStudentRepository repository)
        {
            _repository = repository;
        }

        public void Handle(CreateStudent message)
        {
            var student = new Student
            {
                FirstName = message.FirstName,
                LastName = message.LastName,
                Age = message.Age
            };

            _repository.Add(student);
            _repository.SaveChanges();
        }

        public void Handle(EditStudent message)
        {
            var student = new Student
            {
                FirstName = message.FirstName,
                LastName = message.LastName,
                Age = message.Age
            };

            _repository.Add(student);
            _repository.SaveChanges();
        }

        public void Handle(AssignTeacherToStudent message)
        {
            message.Student.Teacher = message.Teacher;
            _repository.Update(message.Student);
        }
    }

    public class TeacherCommandHandler
    {
        private readonly ITeacherRepository _repository;

        public TeacherCommandHandler(ITeacherRepository repository)
        {
            _repository = repository;
        }

        public void Handle(CreateTeacher message)
        {
            var teacher = new Teacher
            {
                FirstName = message.FirstName,
                LastName = message.LastName,
                Age = message.Age
            };

            _repository.Add(teacher);
            _repository.SaveChanges();
        }

        public void Handle(EditTeacher message)
        {
            var teacher = new Teacher
            {
                FirstName = message.FirstName,
                LastName = message.LastName,
                Age = message.Age
            };

            _repository.Add(teacher);
            _repository.SaveChanges();
        }
    }
}
