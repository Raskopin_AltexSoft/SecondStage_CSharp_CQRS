﻿namespace School.CQRS.Commands
{
    public class CreateStudent : Command
    {
        public readonly string FirstName;
        public readonly string LastName;
        public readonly int Age;
        public CreateStudent(string firstName, string lastName, int age)
        {
            FirstName = firstName;
            LastName = lastName;
            Age = age;
        }
    }
}
