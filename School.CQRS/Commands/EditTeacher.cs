﻿namespace School.CQRS.Commands
{
    public class EditTeacher : Command
    {
        public readonly string FirstName;
        public readonly string LastName;
        public readonly int Age;
        public EditTeacher(string firstName, string lastName, int age)
        {
            FirstName = firstName;
            LastName = lastName;
            Age = age;
        }
    }
}
