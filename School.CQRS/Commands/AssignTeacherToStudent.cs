﻿using School.Data.Entity.Models;

namespace School.CQRS.Commands
{
    public class AssignTeacherToStudent : Command
    {
        public readonly Student Student;
        public readonly Teacher Teacher;
        public AssignTeacherToStudent(Student student, Teacher teacher)
        {
            Student = student;
            Teacher = teacher;
        }
    }
}
