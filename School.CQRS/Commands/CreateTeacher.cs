﻿namespace School.CQRS.Commands
{
    public class CreateTeacher : Command
    {
        public readonly string FirstName;
        public readonly string LastName;
        public readonly int Age;
        public CreateTeacher(string firstName, string lastName, int age)
        {
            FirstName = firstName;
            LastName = lastName;
            Age = age;
        }
    }
}
