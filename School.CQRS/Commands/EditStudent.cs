﻿namespace School.CQRS.Commands
{
    public class EditStudent : Command
    {
        public readonly string FirstName;
        public readonly string LastName;
        public readonly int Age;
        public EditStudent(string firstName, string lastName, int age)
        {
            FirstName = firstName;
            LastName = lastName;
            Age = age;
        }
    }
}
