﻿using School.CQRS.Commands;

namespace School.CQRS.Interfaces
{
    public interface ICommandSender
    {
        void Send<T>(T command) where T : Command;

    }
}
