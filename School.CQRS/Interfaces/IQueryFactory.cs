﻿using School.Data.Entity.Models;

namespace School.CQRS.Interfaces
{
    public interface IQueryFactory
    {
        Teacher[] GetAllTeachers();

        Student[] GetAllStudents();

        Student[] GetStudentsWithoutTeacher(Teacher teacher);

        Student[] GetStudentsDetailed();

        Teacher[] GetTeachersDetailed();
    }
}
